import { Component, Output, EventEmitter } from '@angular/core';

import { TodoService } from '../todos/todo.service';
import { Todo } from '../todos/todo';

@Component({
  selector: 'app-todo-form-smart',
  templateUrl: './todo-form-smart.component.html'
})
export class TodoFormSmartComponent {
  constructor(private todoService: TodoService) {}

  addTodo(item: { label: string }) {
    this.todoService.addTodo(item);
  }
}
