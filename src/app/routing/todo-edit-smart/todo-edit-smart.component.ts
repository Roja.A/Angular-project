import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TodoService } from '../todos/todo.service';
import { Todo } from '../todos/todo';
@Component({
  selector: 'app-todo-edit-smart',
  templateUrl: './todo-edit-smart.component.html'
})
export class TodoEditSmartComponent implements OnInit {
  selectedTodo;
  constructor(
    private route: ActivatedRoute,
    private todoService: TodoService
  ) {}
  ngOnInit() {
    this.route.params.subscribe(params => {
      const todo = this.todoService.findById(parseInt(params.id, 10));
      this.selectedTodo = todo;
    });
  }

  updateTodo(todo) {
    this.todoService.update(todo);
  }
}
