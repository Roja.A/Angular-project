import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { TodosComponent } from './routing/todos/todos.component';
import { TodoComponent } from './routing/todos/todo.component';
import { TodoFormComponent } from './routing/todos/todo-form.component';
import { TodoListComponent } from './routing/todos/todo-list.component';
import { TodoService } from './routing/todos/todo.service';
import { AppComponent } from './app.component';
// import { TodosComponent } from './todos/todos.component';
// import { TodoComponent } from './todos/todo.component';
// import { TodoFormComponent } from './todos/todo-form.component';
// import { TodoListComponent } from './todos/todo-list.component';
import { CounterComponent } from './counter/counter.component';
import { SignupFormComponent } from './forms/signup-form.component';
// import { TodoService } from './todos/todo.service';
import { RoutingComponent } from './routing/routing.component';
import { TodoFormSmartComponent } from './routing/todo-form-smart/todo-form-smart.component';
import { TodoEditComponent } from './routing/todo-edit/todo-edit.component';
import { TodoEditSmartComponent } from './routing/todo-edit-smart/todo-edit-smart.component';

// import { SampleComponent } from './sample/sample.component';
export const ROUTES: Routes = [
  { path: 'todos', component: TodosComponent },
  { path: 'todo/new', component: TodoFormSmartComponent },
  { path: 'todos/:id/edit', component: TodoEditSmartComponent }
];
@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(ROUTES), FormsModule],
  declarations: [
    AppComponent,
    TodosComponent,
    TodoComponent,
    TodoFormComponent,
    TodoListComponent,
    TodoEditSmartComponent,
    TodoEditComponent,
    TodoFormSmartComponent,
    CounterComponent,
    SignupFormComponent,
    RoutingComponent
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule {}
