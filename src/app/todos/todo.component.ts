import { Component, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'todo',
  styleUrls: ['./todo.component.css'],
  templateUrl: './todo.component.html'
})
export class TodoComponent {
  @Input() item;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChange = new EventEmitter();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onRemove = new EventEmitter();
}
