import { Component } from '@angular/core';
import { TodoService } from './todo.service';
import { Todo } from './todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html'
})
export class TodosComponent {
  todos: Todo[];
  constructor(private todoService: TodoService) {
    this.todos = this.todoService.getTodos();
  }

  addTodo(item: { label: string }) {
    this.todoService.addTodo(item);
    this.todos = this.todoService.getTodos();
  }

  completeTodo(item: { todo: Todo }) {
    this.todoService.completeTodo(item.todo);
    this.todos = this.todoService.getTodos();
  }

  removeTodo(item: { todo: Todo }) {
    this.todoService.removeTodo(item.todo);
    this.todos = this.todoService.getTodos();
  }
}
