import { Injectable } from '@angular/core';

import { Todo } from './todo';
export class TodoService {
  todos: Todo[] = [
    {
      label: 'Eat pizza',
      id: 0,
      complete: true
    },
    {
      label: 'Do some coding',
      id: 1,
      complete: true
    },
    {
      label: 'Sleep',
      id: 2,
      complete: false
    },
    {
      label: 'Print tickets',
      id: 3,
      complete: true
    }
  ];
  getTodos() {
    return this.todos;
  }
  addTodo(item: { label: string }) {
    this.todos = [
      {
        label: item.label,
        id: this.todos.length + 1,
        complete: false
      },
      ...this.todos
    ];
  }

  completeTodo(todo) {
    this.todos = this.todos.map(
      item => (item.id === todo.id ? { ...item, complete: true } : item)
    );
  }

  removeTodo(todo) {
    this.todos = this.todos.filter(it => it.id !== todo.id);
  }
}
