import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'todo-form',
  templateUrl: './todo-form.component.html'
})
export class TodoFormComponent {
  label: string;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onAdd = new EventEmitter();

  submit() {
    if (!this.label) {
      return;
    }
    this.onAdd.emit({ label: this.label });
    this.label = '';
  }
}
