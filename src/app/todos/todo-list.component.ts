import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from './todo';

@Component({
  selector: 'todo-list',
  templateUrl: './todo-list.component.html'
})
export class TodoListComponent {
  @Input() todos: Todo[];
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onComplete = new EventEmitter();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onDelete = new EventEmitter();
}
