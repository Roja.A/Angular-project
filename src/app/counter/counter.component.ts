import { Component, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-counter',
  styleUrls: ['./counter.component.css'],
  templateUrl: './counter.component.html'
})
export class CounterComponent {
  @Input('initOne') counter = 0;

  @Output('update') change: EventEmitter<number> = new EventEmitter<number>();
  increment() {
    this.counter++;
    this.change.emit(this.counter);
  }
  decrement() {
    this.counter--;
    this.change.emit(this.counter);
  }
}
