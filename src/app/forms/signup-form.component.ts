import { Component } from '@angular/core';
import { User } from './signup.interface';

@Component({
  selector: 'app-signup-form',
  styleUrls: ['./signup.component.css'],
  templateUrl: './signup.component.html'
})
export class SignupFormComponent {
  user: User = {
    name: '',
    account: {
      email: '',
      confirm: ''
    }
  };
  onSubmit({ value, valid }: { value: User; valid: boolean }) {
    console.log(value, valid);
  }
}
